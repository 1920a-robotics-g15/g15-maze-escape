import socket
import time
import tkinter as tk


serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serv.bind(('192.168.1.169', 8080))
serv.listen(5)

def key_input(event):
    key_press = event.keysym.lower()
    print(key_press)
 
    if key_press == 'w':
        msg = 'forward'
        byte = msg.encode()
        conn.send(byte)
        time.sleep(0.1)
    elif key_press == 'a':
        msg = 'left'
        byte = msg.encode()
        conn.send(byte)
        time.sleep(0.1)
    elif key_press == 'd':
        msg = 'right'
        byte = msg.encode()
        conn.send(byte)
        time.sleep(0.1)
    elif key_press == 's':
        msg = 'backward'
        byte = msg.encode()
        conn.send(byte)
        time.sleep(0.1)
    elif key_press == 'space':
        msg = 'stop'
        byte = msg.encode()
        conn.send(byte)
        time.sleep(0.1)

while True:
    conn, addr = serv.accept()
    command = tk.Tk()
    command.bind_all('w', key_input)
    command.bind_all('a', key_input)
    command.bind_all('d', key_input)
    command.bind_all('s', key_input)
    command.bind_all('<space>', key_input)
    command.mainloop()
        
    #conn.close()
    #print("client disconnected")