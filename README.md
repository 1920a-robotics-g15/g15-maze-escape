### G15-maze-escape

* Marko Muro
* Kaire Koljal
* Hans P�rtel Pani
* Kaarel Loide

### Project overview
The purpose of this project is to navigate a GoPiGo3 robot autonomously through a maze. The maze is specified as black lines on a white background, with spaces large enough for the GoPiGo3 robot to move inside. Either a white (light colored) floor or white paper is needed to construct the appropriate maze. There is a single exit to the maze and one or more valid paths leading to it from the starting position. For tracking purposes the robot will be marked with a color or a shape. Given these conditions, the robot will exit the maze following the shortest valid path. To achieve this, a camera connected to a separate computer from the robot will be overlooking the setup from above. A live feed from the camera will be used. First, the maze will be mapped, then the robot will be located in the maze and the shortest path to the exit will be found. The path will be turned into movement instructions for the robot, and the instructions will be relayed to the robot. While moving through the maze, the position of the robot will be continuously monitored to detect any errors, such as the robot not staying within bounds.

### Schedule

* Map the maze - **50h** `due @ 20.11.2019`
    * Compare different maze mapping approaches
    * Detect the maze
    * Map the maze
    * Find a path to the exit from any starting position
* Locate the robot - **50h** `due @ 27.11.2019`
    * Compare and pick a solution to track the robot sufficiently
    * Detect the robot
    * Keep track of the robot as it moves
* Demo video; challenges and solutions and project poster - **16h** `due @ 04.12.2019`
* Robot navigation out of the maze - **100h** `due @ 11.12.2019`
    * Communication between the robot and the image processing computer
    * Make the robot move to a given location on the image
    * Convert the maze exit path into driving instructions
	* Move the robot based on generated instructions
    * Detect and correct any robot movement errors
* Final presentation: live demo - **20h** `due @ 18.12.2019`
    * Prepare the presentation
    * Prepare the maze
    * Ensure the solution works in the presentation environment


### Component list
| Item                               | Link to the item                                             | You will provide | Need from instructors | Total |
| ---------------------------------- | ------------------------------------------------------------ | ---------------: | --------------------: | :---: |
| GoPiGo 3 robot (with Raspberry Pi) | https://shop.dexterindustries.com/gopigo-beginner-starter-kit/ |                0 |                     1 |   1   |
| MicroSD card with an adapter       | https://www.amazon.co.uk/SanDisk-microSDXC-Memory-Adapter-Performance/dp/B073JYVKNX |                0 |                     1 |   1   |
| Raspberry Pi power adapter         | https://www.insplay.eu/et/product/raspberry-pi-3-micro-usb-laadija-5v-25a |                0 |                     1 |   1   |
| LiPo battery                       | https://www.droonimaailm.ee/toode/gens-ace-2200mah-11-1v-30c-3s1p-lipo-battery/ |                0 |                     2 |   2   |
| GoPiGo power cable for LiPo batter |                                                              |                0 |                     1 |   1   |
| LiPo battery alarm                 | https://www.droonimaailm.ee/toode/1s-8s-dual-speaker-lipo-alarm-8s-voltage-tester |                0 |                     1 |   1   |
| Logitech C920 camera               | https://www.amazon.com/Logitech-Widescreen-Calling-Recording-Desktop/dp/B006JH8T3S |                0 |                     1 |   1   |
| Laptop                             |                                                              |                0 |                     1 |   1   |
| Black tape                         | https://www.amazon.co.uk/Insulation-Tape-Electrical-19mm-Black/dp/B004CSC4W8 |                0 |                     5 |   5   |
| White paper                        | https://www.amazon.co.uk/Club-Drawing-Paper-Pad-A3/dp/B0050OHLJO |                0 |                     2 |   2   |


### Challenges and Solutions

#### Finding the location and orientation of the robot (Kaarel Loide)

We started from finding the orientation since that seemed like the more difficult part of this task. We had some ideas on how to find the orientation like using some shape like a triangle for example. After some thought we realized that this might not be the best approach. The next idea was to use 2 different colored circles but that would mean some extra image manipulation logic so we discarded this idea as well.
Finaly we found a solution called ArUco markers that has support in OpenCV and seemed like a perfect solution that also happens to solve the location part of the task. We used OpenCV to detect the marker and then did some simple calculations using the found corner coordinates to find the orientation.

#### Making maze detection and robot detection work together and send commands to the robot (Kaire Koljal)

Conditions that have to be met: detecting the robot and solving the maze as well as communication between the robot and computer has to work. 
maze_solver.py needs to use camera feed instead of pictures. For that, I have created a separate file (program.py) that contains the code that was in robot_detection.py and it feeds camera frames to functions in maze_solver.py to find the exit. 
To solve the maze from camera frame, I also need to remove robot from the frame. This could be done by assessing the size of the marker and then replacing robot and marker pixels with correct maze pixels. It needs to be tested out and will be used unless we find a better solution. (It means that the ArUco marker needs to be a bit bigger than in the video to get a better estimate of the size of the robot.) 
The relation between path coordinates and robot coordinates along with direction has to be converted into commands for the robot and sent to it. (Since the exit path is a list of coordinates the following will be similar to line following tasks done in labs.)


#### Detecting the maze (Hans Pani)

Need to come up with a way to find the exit from the maze, it works on static images, we'll test and modify if it doesn't work with an actual camera feed.

#### Communication between robot and laptop (Marko Muro)

At first the plan was to start ROS for the whole communication operation, but after further investigation that task seemed a bit too hard - a whole new platform would have to be made clear as how things work. Instead of it socket programming approach was selected which made things a lot simpler, easier and quicker to implement. In order to send data over wifi between client and server, data had to be encoded aswell - figuring that out was a small obstacle. Although robot commands can be sequentially read from file at the moment, this is not ideal - need to find the best way how to feed maze solver data directly to comms socket transfer in real time.