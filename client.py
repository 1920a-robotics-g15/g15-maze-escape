import easygopigo as go
import time
import socket
myRobot = go.EasyGoPiGo()
myRobot.set_speed(100)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(('192.168.1.169', 8080))

def move(text):
    print('myRobot.'+text+'()')
while True:
    from_server = client.recv(4096)
    text = from_server.decode()
    move(text)
    if text == 'forward':
        myRobot.forward()
    elif text == 'backward':
        myRobot.backward()
    elif text == 'right':
        myRobot.right()
    elif text == 'left':
        myRobot.left()
    elif text == 'stop':
        myRobot.stop()

