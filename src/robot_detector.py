from aruco_detector import *

# To test this open http://chev.me/arucogen/ on a smartphone for example
# and generate a 6x6 marker with any id from 0 to 49, then show it to the camera.
cap = cv2.VideoCapture(0)

while True:
    # capture frame-by-frame
    ret, frame = cap.read()
    # get markers
    markers = detect(frame)
    # get angles
    angles = calculate_orientations(markers)
    # add markings to images
    img = mark(frame, markers, angles)
    cv2.imshow("detection", img)
    # close when q is pressed
    if cv2.waitKey(1) == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
