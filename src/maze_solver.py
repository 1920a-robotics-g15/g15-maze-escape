import time
from collections import deque
from glob import glob
from operator import itemgetter
from typing import Dict, Tuple, List, Iterator

import cv2 as cv
import numpy as np
import pandas as pd

from maze_detector import process_frame


def get_adjacent_pixels(pixel: Tuple[int, int],
                        height: int,
                        width: int,
                        visited_coordinates: List[Tuple[int, int]]) -> List[Tuple[int, int]]:
    """

    :param pixel: pixel coordinates
    :param height: frame height
    :param width: frame width
    :param visited_coordinates: list[(y, x)] of already visited coords
    :return: list[(y, x)] coordinates for adjacent pixels
    """
    y = pixel[0]
    x = pixel[1]
    adj_straight = [(y - 1, x), (y, x - 1), (y, x + 1), (y + 1, x)]
    adj_diagonal = [(y - 1, x - 1), (y - 1, x + 1), (y + 1, x - 1), (y + 1, x + 1)]
    adj = adj_straight + adj_diagonal
    return list(filter(lambda px: 0 <= px[0] < height and 0 <= px[1] < width and px not in visited_coordinates, adj))


def unravel_prev_pixel_dict(prev_pixel_dict: Dict[Tuple[int, int], Tuple[int, int]],
                            final_pixel: Tuple[int, int]) -> List[Tuple[int, int]]:
    """

    :param prev_pixel_dict: coordinate: coordinate dictionary {(y: x) : (y: x)}
    :param final_pixel: the pixel which found the exit (y, x)
    """
    current = final_pixel
    while current in prev_pixel_dict:
        yield current
        current = prev_pixel_dict[current]


def breadth_first_search(frame: np.ndarray,
                         starting_coordinates: (int, int)) -> [(int, int)]:
    """

    :param frame: processed frame
    :param starting_coordinates: starting coordinates (y, x) in the coordinate space of the processed frame
    :return: list[(y, x)] of coordinates starting from the starting coordinates
    """
    height, width = frame.shape

    prev_pixel_dict = {starting_coordinates: None}  # { pixel: prev pixel }
    visited_coordinates = [starting_coordinates]

    y, x = starting_coordinates
    queue = deque([(y, x, time.perf_counter(), frame[y, x])])

    while queue:
        queue = deque(sorted(queue, key=itemgetter(2)))  # sort by insertion time
        queue = deque(sorted(queue, key=itemgetter(3)))  # sort by pixel value
        y, x, queueing_time, value = queue.popleft()
        if y == 0 or x == 0 or y == height - 1 or x == width - 1:
            return list(reversed(list(unravel_prev_pixel_dict(prev_pixel_dict, (y, x)))))
        for pixel in get_adjacent_pixels((y, x), height, width, visited_coordinates):
            prev_pixel_dict[pixel] = (y, x)
            visited_coordinates.append(pixel)
            queue.append((pixel[0], pixel[1], time.perf_counter(), frame[pixel[0], pixel[1]]))
            pass


def reduce_coordinates(coordinates: List[Tuple[int, int]]) -> Iterator[Tuple[int, int]]:
    """

    :param coordinates: list[(y, x)] of input coordinates
    :return: reduced coordinate list[(y, x)] generator
    """
    for i in range(len(coordinates)):
        prev_c = coordinates[i - 1] if i > 0 else None
        cur_c = coordinates[i]
        next_c = coordinates[i + 1] if i + 1 < len(coordinates) else None

        slope_to_prev = None
        if prev_c:
            prev_dy = prev_c[0] - cur_c[0]
            prev_dx = prev_c[1] - cur_c[1]
            slope_to_prev = prev_dy / prev_dx if prev_dx != 0 else None

        slope_to_next = None
        if next_c:
            next_dy = next_c[0] - cur_c[0]
            next_dx = next_c[1] - cur_c[1]
            slope_to_next = next_dy / next_dx if next_dx != 0 else None

        if slope_to_prev == slope_to_next:
            continue
        yield cur_c


def translate_coordinates(coordinates: List[Tuple[int, int]],
                          scaling_factor: int) -> List[Tuple[int, int]]:
    """

    :param coordinates: list[(y, x)] of input coordinates
    :param scaling_factor: how much the frame was resized by
    :return: list[(y, x)] of translated coordinates
    """
    return list(
        map(lambda c: (c[0] * scaling_factor, c[1] * scaling_factor), coordinates))


def get_maze_exit_path(frame: np.ndarray,
                       start: Tuple[int, int]) -> List[Tuple[int, int]]:
    """

    :param frame: input frame of just the maze
    :param start: starting coordinates on the input frame
    :return: list of coordinates from start to exit
    """
    processed_frame, scaling_factor = process_frame(frame)
    #cv.imshow('processed_frame0', processed_frame)
    path = breadth_first_search(processed_frame, (int(start[0] / scaling_factor), int(start[1] / scaling_factor)))
    reduced_path = list(reduce_coordinates(path)) # some path points from beginning go missing
    #translated_path = translate_coordinates(reduced_path, scaling_factor)
    translated_path = translate_coordinates(path, scaling_factor)
    return translated_path


def draw_coordinates(frame: np.ndarray,
                     coordinates: List[Tuple[int, int]],
                     line_color: Tuple[int, int, int] = (0, 255, 0),
                     line_thickness: int = 2,
                     point_color: Tuple[int, int, int] = (0, 255, 255),
                     point_thickness: int = 8) -> None:
    """

    :param frame: original frame to draw coordinates onto
    :param coordinates:  list[(y, x)] of input coordinates
    :param line_color: color of the line (b, g, r)
    :param line_thickness:
    :param point_color: color of the points (b, g, r)
    :param point_thickness:
    """
    prev = None
    for coordinate in coordinates:
        c_point = (coordinate[1], coordinate[0])
        if prev:
            p_point = (prev[1], prev[0])
            cv.line(frame, p_point, c_point, line_color, line_thickness)

        cv.line(frame, c_point, c_point, point_color, point_thickness)
        prev = coordinate


def main():
    pd.set_option('display.width', 320)  # console print width
    np.set_printoptions(linewidth=320)

    maze_images = glob('../img/maze_*.jpg')
    frame = cv.imread(maze_images[7])

    start = time.perf_counter()
    path = get_maze_exit_path(frame, (360, 400))
    print(f'took {1000 * (time.perf_counter() - start)}ms')

    draw_coordinates(frame, path)
    cv.imshow('frame', frame)
    cv.waitKey(0)


if __name__ == "__main__":
    main()
