import cv2
import cv2.aruco as aruco
from math import atan, pi
import numpy


def detect(img):
    # detect ArUco markers
    detected_markers = dict()
    # use 6 by 6 markers
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_50)
    parameters = aruco.DetectorParameters_create()
    corners, ids, _ = aruco.detectMarkers(img, aruco_dict, parameters=parameters)

    # add corners to dictionary with id of the marker as key
    if numpy.all(ids is not None):
        for i, j in zip(ids, corners):
            detected_markers[i[0]] = j[0]

    return detected_markers


def calculate_angle(x1, y1, x2, y2):
    # get angle between 2 coordinates
    val = (y2 - y1) / (x2 - x1)
    val = atan(abs(val))
    val = (int(val * 180 / pi)) % 360
    val = 360 - val
    if x2 < x1 and y2 > y1:
        val = (180 - val) % 360
    elif x2 < x1 and y1 > y2:
        val = (180 + val) % 360
    elif x2 > x1 and y2 < y1:
        val = (360 - val) % 360
    return val


def get_line_coords(detected_markers, key):
    # first corner
    x = detected_markers[key][0][0]
    y = detected_markers[key][0][1]
    # second corner
    x1 = detected_markers[key][1][0]
    y1 = detected_markers[key][1][1]
    # third corner
    x2 = detected_markers[key][2][0]
    y2 = detected_markers[key][2][1]
    # find top middle and center coordinate from the corners
    # 1st: center x, 2nd: center y, 3rd: top-mid x, 4th: top-mid y
    return 0.5 * abs(x2 + x), 0.5 * abs(y2 + y), 0.5 * abs(x1 + x), 0.5 * abs(y1 + y)


def calculate_orientations(detected_markers):
    # calculate orientations for all detected markers and add them to a dictionary
    angles = dict()
    for key in detected_markers.keys():
        x1, y1, x2, y2 = get_line_coords(detected_markers, key)
        angles[key] = calculate_angle(x1, y1, x2, y2)

    return angles


def mark(img, detected_markers, angles):
    # add angle, marker index and orientation line to image
    font = cv2.FONT_HERSHEY_SIMPLEX
    red = (0, 0, 255)
    green = (0, 255, 0)
    blue = (255, 0, 0)

    for key, angle in zip(detected_markers.keys(), angles.values()):
        x1, y1, x2, y2 = get_line_coords(detected_markers, key)
        img = cv2.circle(img, (int(x1), int(y1)), 4, red, -1)
        img = cv2.line(img, (int(x1), int(y1)), (int(x2), int(y2)), blue, 3)
        cv2.putText(img, str(angle), (int(x1 - 70), int(y1)), font, 1, green, 2)
        cv2.putText(img, str(key), (int(x1 + 30), int(y1)), font, 1, red, 2)

    return img
