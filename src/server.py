
import socket
import gopigo as go

# From lab10 server.py

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
go.set_speed(50)

try:
    
    PORT_NR = 6673
    server_socket.bind(('', PORT_NR))

    while 1:
        dataFromClient, address = server_socket.recvfrom(256)  # Receive data from client
        dataFromClient = dataFromClient.decode("utf-8").strip()  # To avoid any errors in comparison when a line ending is appended
        #print(dataFromClient)  # To see what is received
        if dataFromClient == 'F':
            print("fwd")
            go.fwd()
        elif dataFromClient == 'S':
            print("stop")
            go.stop()
        elif dataFromClient == 'L':
            print("left")
            go.left_rot()
        elif dataFromClient == 'R':
            print("right")
            go.right_rot()
finally:
    go.stop()
    server_socket.close()
