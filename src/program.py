# Main program: start detecting & solving the maze (and send commands to the robot)
import socket
import time
from math import sqrt, degrees, atan2
import numpy as np

from maze_solver import *       
from aruco_detector import *    

####################################################################


# from lab10 Drive.py
def getCommand(markers, path, robotID, center_x, center_y, next):
    global current
    command = None

    front_x = int ((markers[robotID][0][0] + markers[robotID][1][0])/2)
    front_y = int ((markers[robotID][0][1] + markers[robotID][1][1])/2)

    while sqrt((current[1] - center_x) ** 2 + (current[0] - center_y) ** 2) < 15:  # When close enough to destination point, then take next point
        next += 1
        if next == len(path) - 1 or next >= len(path): 
            command = 'S'
            return next, command
        current = path[next]  
    
    len_to_next = sqrt((current[0] - center_x) ** 2 + (current[1] - center_y) ** 2)

    if len_to_next > 100:  
        miss = 10 
    elif len_to_next < 30: 
        miss = 30
    else:
        miss = 30 - 25*(len_to_next-30)/70  # Make linear function between 100 and 30 pixel distance
    
    # robot vector
    x1 = front_x - center_x 
    y1 = front_y - center_y  
    # robot center to next destination vector
    x2 = current[1]  - center_x  
    y2 = current[0]  - center_y  
    
    dot = x1 * x2 + y1 * y2  
    det = x1 * y2 - y1 * x2  
    angle = degrees(atan2(det, dot))
    
    if angle > miss:
        command = 'R' #right
    elif angle < -miss:
        command = 'L' #left
    else:
        command = 'F' #forward
    return next, command


####################################################################

if __name__ == "__main__":
    pd.set_option('display.width', 320)  # console print width
    np.set_printoptions(linewidth=320)
    #default camera frame is 480x640

    # To test markers open http://chev.me/arucogen/ on a smartphone for example
    # and generate a 6x6 marker with any id from 0 to 49, then show it to the camera.
    cap = cv2.VideoCapture(1) 

    detected = False
    # marker center coordinates
    center_x = None
    center_y = None

    robotID = 8
    ip = "192.168.1.65"
    port = 6673

    # find path once, when robot is detected
    while True:
        ret, f = cap.read()
        frame = f
        markers = detect(frame)
        # the first frame is not good enough, since the camera still adjusts to the brightness of the room
        if markers:
            i=0
            while i<50:
                ret, frame = cap.read()
                i+=1
            markers = detect(frame)
            detected = True
            # centre coordinates of the marker
            center_x = int ((markers[robotID][0][0] + markers[robotID][2][0])/2)
            center_y = int ((markers[robotID][0][1] + markers[robotID][2][1])/2)
            break
    
    # start solving maze
    if detected:
        start = time.perf_counter()
        # if we have marker center coordinates then find path
        if center_x != None :
            # erasing marker from frame while finding the path
            points = np.array(markers[robotID], np.int32)
            cv2.fillConvexPoly(frame, points, (255,255,255))
            #cv.imshow('marker erased', frame)
            # gets path to exit strating from robot coordinates
            path = get_maze_exit_path(frame, (center_y, center_x))
            # draws the exit path on frame
            draw_coordinates(frame, path)
        cv.imshow('Path', frame)
    
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # Open a client socket
    # path points
    next = 0
    current = path[0]
    try:
        while True:
            ret, f = cap.read()
            #frame = f  # camera frame
            markers = detect(f)  # get markers
            
            if markers: # if robot/marker was detected
                # centre coordinates of the marker
                center_x = int ((markers[robotID][0][0] + markers[robotID][2][0])/2)
                center_y = int ((markers[robotID][0][1] + markers[robotID][2][1])/2)
                
                # to see marker detection on frame
                angles = calculate_orientations(markers)
                f = mark(f, markers, angles)
                
                # get commands for moving
                next, command = getCommand(markers, path, robotID, center_x, center_y, next)
                
                if command != None:
                    #pass
                    client_socket.sendto(command.encode('utf-8'), (ip, port)) # send command to robot
                if command == 'S': # we probably made it to the exit
                    command = 'F'   # drive off the maze
                    client_socket.sendto(command.encode('utf-8'), (ip, port))
                    time.sleep(3)
                    command = 'S'
                    client_socket.sendto(command.encode('utf-8'), (ip, port))
                    break
                time.sleep(0.1)

                # close when q is pressed
                if cv2.waitKey(1) == ord('q'):
                    break
                
            else:  # if we don't see marker anymore, stop  (just in case)
                command = 'S'  
                client_socket.sendto(command.encode('utf-8'), (ip, port))
            
            cv2.imshow("robot detection", f)
            
    finally:
        command = 'S'
        client_socket.sendto(command.encode('utf-8'), (ip, port))
        client_socket.close()
        cap.release()
        cv.destroyAllWindows()
