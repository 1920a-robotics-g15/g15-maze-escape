import time
from glob import glob
from math import ceil
from typing import Tuple

import cv2 as cv
import numpy as np
import pandas as pd


def pad_and_resize(frame: np.ndarray,
                   longer_side_len: int) -> Tuple[np.ndarray, int, int, int]:
    """

    :param frame: input frame
    :param longer_side_len: longest side length after resize
    :return:  resized frame, scaling factor, padding added to the height/bottom, padding added to the right/width
    """
    f_h = frame.shape[0]
    f_w = frame.shape[1]
    longer_side = max(f_h, f_w)
    scaling_factor = ceil(longer_side / longer_side_len)
    longer_side_padded = scaling_factor * longer_side_len
    shorter_side = min(f_h, f_w)
    shorter_side_padded = ceil(shorter_side / scaling_factor) * scaling_factor

    longer_side_to_pad = longer_side_padded - longer_side
    shorter_side_to_pad = shorter_side_padded - shorter_side
    h_to_pad = longer_side_to_pad if longer_side == f_h else shorter_side_to_pad
    w_to_pad = longer_side_to_pad if longer_side == f_w else shorter_side_to_pad

    # numpy pad order [(top, bottom), (left, right)]
    frame_processed = np.pad(frame, [(0, h_to_pad), (0, w_to_pad)], 'constant', constant_values=255)

    longer_side_resized = int(longer_side_padded / scaling_factor)
    shorter_side_resized = int(shorter_side_padded / scaling_factor)
    h_resized = longer_side_resized if longer_side == f_h else shorter_side_resized
    w_resized = longer_side_resized if longer_side == f_w else shorter_side_resized

    frame_processed = cv.resize(frame_processed, (w_resized, h_resized), interpolation=cv.INTER_LINEAR)
    return frame_processed, scaling_factor, h_to_pad, w_to_pad


def process_frame(frame: np.ndarray) -> Tuple[np.ndarray, int]:
    """
    :param frame: input frame
    :return: processed frame, scaling factor
    """
    
    frame_processed = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    frame_processed = cv.GaussianBlur(frame_processed, (5, 5), 0)  # remove noise
    frame_processed, scaling_factor, h_to_pad, w_to_pad = pad_and_resize(frame_processed, longer_side_len=128)

    f_h = frame_processed.shape[0]
    approx_wall_width_constant = 0.0375  # height/tape_width is between 0.3 and 0.5

    approx_wall_width_open = int(f_h * approx_wall_width_constant * 2)
    open_rect = cv.getStructuringElement(cv.MORPH_RECT, (approx_wall_width_open, approx_wall_width_open))
    frame_processed = cv.morphologyEx(frame_processed, cv.MORPH_OPEN, open_rect, iterations=1)  # remove reflections on tape

    frame_processed = cv.medianBlur(frame_processed,3)

    approx_wall_width_close = int(f_h * approx_wall_width_constant)
    close_rect = cv.getStructuringElement(cv.MORPH_RECT, (approx_wall_width_close, approx_wall_width_close))
    frame_processed = cv.morphologyEx(frame_processed, cv.MORPH_CLOSE, close_rect)  # remove small pieces

    approx_wall_width_erode = int(f_h * approx_wall_width_constant * 4) 
    erode_rect = cv.getStructuringElement(cv.MORPH_RECT, (approx_wall_width_erode, approx_wall_width_erode))
    frame_processed = cv.morphologyEx(frame_processed, cv.MORPH_ERODE, erode_rect)  # make the lines thicker
    ret, frame_processed = cv.threshold(frame_processed, 80, 255, cv.THRESH_BINARY)
    #cv.imshow("frame_processed1", frame_processed)
    frame_processed = cv.bitwise_not(frame_processed)
    frame_processed = cv.blur(frame_processed, (7, 7))  # smooth the paths to make it less likely to pick edges
    #cv.imshow("frame_processed2", frame_processed)
    frame_processed, scaling_factor_2, h_to_pad_2, w_to_pad_2 = pad_and_resize(frame_processed, longer_side_len=32)
    #cv.imshow("frame_processed3", frame_processed)
    return frame_processed, scaling_factor * scaling_factor_2


def main():
    pd.set_option('display.width', 320)  # edit console print width to print the np array
    np.set_printoptions(linewidth=320)

    maze_images = glob('../img/maze_*.jpg')
    frame = cv.imread(maze_images[6])

    start = time.perf_counter()
    frame, scaling_factor = process_frame(frame)
    print(f'took {1000 * (time.perf_counter() - start)}ms')

    print(frame)
    cv.imshow('frame', frame)

    cv.waitKey(0)


if __name__ == "__main__":
    main()
